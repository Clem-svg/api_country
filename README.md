# api_country

brief 3 Simplon CDA Api rest spring boot

API was made with Spring Boot
Front was made with jQuery, Bootstrap 5.0 & Fontawesome

## Installation

make sure you have Java(11) and Maven(3.8.4 or above) installed

make sure you set your environnement variables on windows (ex: JAVA_HOME : F:\Java\jdk-11, Path : F:\Maven\apache-maven-3.8.4\bin)

`git clone `

make sure to change your database credentials in api_country\src\main\resources\application.properties

`cd /api_country`

type the following command to run the project:
`./mvnw spring-boot:run`

go to :

http://localhost:8080/view-countries to access the front (which consume the API)

http://localhost:8080/api to access the API (this url gets all countries)

http://localhost:8080/template/countries/{id} (get one country)

## Packaged app

.jar is located here : api_country\target\api-0.0.1-SNAPSHOT.jar

run it with the following command :

`java -jar ./target/api-0.0.1-SNAPSHOT.jar`

Now you should be able to see the welcome page of our application when you visit http://localhost:8080/view-countries.

## Curl commands

`$ curl -X GET http://localhost:8080/api`

`$ curl -X GET http://localhost:8080/template/countries/{id}`

`$ curl -X POST http://localhost:8080/template/countries -H 'Content-Type: application/json' -d '{"name":"Espagne","code":"ES"}'`

`$ curl -X PUT http://localhost:8080/template/countries/{id} -H 'Content-Type: application/json' -d '{"name":"update","code":"UP"}'`

`$ curl -X DELETE  http://localhost:8080/template/countries/{id}`

