package com.brief3.api;

public class CountryNotFoundException extends RuntimeException{
     public CountryNotFoundException(String message){
          super(message);
     }
}
