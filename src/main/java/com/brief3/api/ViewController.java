package com.brief3.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

     @RequestMapping("/view-countries")
     public String viewCountries() {
          return "view-countries";
     }

}
