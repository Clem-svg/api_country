package com.brief3.api;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name="country")
public class Country {

     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
     private long id;

     @Column(name="name")
     @NotBlank(message = "Name is mandatory")
     @Size(min = 4, max = 15, message = "Name must be between 4 and 15 characters")
     private String name;

     @Column(name="code")
     @NotBlank(message = "Code is mandatory")
     @Size(min = 2, max = 2, message = "Code must be 2 characters")
     private String code;

     public Country() {

     }

     public Country(String name, String code) {
          this.name = name;
          this.code = code;
     }

     public long getId() {
          return id;
     }
     public String getName() {
          return name;
     }
     public void setName(String name) {
          this.name = name;
     }
     public String getCode() {
          return code;
     }
     public void setCode(String code) {
          this.code = code;
     }
     
	@Override
	public String toString() {
		return "Country [id=" + id + ", name=" + name + ", code=" + code + "]";
	}
}
