package com.brief3.api;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


import com.brief3.api.Country;
import com.brief3.api.CountryNotFoundException;

@RestController
public class APIController {
     @Autowired
     RestTemplate restTemplate;

     private final CountryRepository repository;

     APIController(CountryRepository repository) {
          this.repository = repository;
     }

     @GetMapping("/api")
     List<Country> all() {
          if (repository.findAll().isEmpty()) {
               throw new CountryNotFoundException("Country not found");  
          } else {
               return repository.findAll();
          }
     }
          
     // @RequestMapping(value = "/template/countries")
     // public String getCountryList() {
     //      HttpHeaders headers = new HttpHeaders();
     //      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
     //      HttpEntity<String> entity = new HttpEntity<String>(headers);
     
     //      return restTemplate.exchange(
     //      "http://localhost:8080/countries", HttpMethod.GET, entity, String.class).getBody();
     // }

     @GetMapping("/template/countries/{id}")
     Optional<Country> getCountry(@PathVariable("id") Long id) {
          
          if (repository.findById(id).isEmpty()) {
               throw new CountryNotFoundException("Country with id : "+id+" does not exists");
          } else {
               return repository.findById(id);
          }
     }
     // @RequestMapping(value = "/template/countries/{id}")
     // public String getCountry(@PathVariable("id") String id) {
     //      HttpHeaders headers = new HttpHeaders();
     //      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
     //      HttpEntity<String> entity = new HttpEntity<String>( headers);
          
     //      return restTemplate.exchange(
     //           "http://localhost:8080/countries/"+id, HttpMethod.GET, entity, String.class).getBody();
     // }

     @RequestMapping(value = "/template/countries", method = RequestMethod.POST)
     public String createCountries(@RequestBody Country country) {
          HttpHeaders headers = new HttpHeaders();
          headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
          HttpEntity<Country> entity = new HttpEntity<Country>(country, headers);
     
          return restTemplate.exchange(
          "http://localhost:8080/countries", HttpMethod.POST, entity, String.class).getBody();
     }

     @RequestMapping(value = "/template/countries/{id}", method = RequestMethod.PUT)
     public String updateCountry(@PathVariable("id") String id, @RequestBody Country country) {
          HttpHeaders headers = new HttpHeaders();
          headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
          HttpEntity<Country> entity = new HttpEntity<Country>(country, headers);
     
          return restTemplate.exchange(
          "http://localhost:8080/countries/"+id, HttpMethod.PUT, entity, String.class).getBody();
     }

     @RequestMapping(value = "/template/countries/{id}", method = RequestMethod.DELETE)
     public String deleteCountry(@PathVariable("id") String id) {
          HttpHeaders headers = new HttpHeaders();
          headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
          HttpEntity<Country> entity = new HttpEntity<Country>(headers);
     
          return restTemplate.exchange(
          "http://localhost:8080/countries/"+id, HttpMethod.DELETE, entity, String.class).getBody();
     }
}
