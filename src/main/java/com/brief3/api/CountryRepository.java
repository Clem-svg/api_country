package com.brief3.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brief3.api.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {

}